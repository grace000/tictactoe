// Load the NPM Package inquirer
var inquirer = require("inquirer");

// Create a "Prompt" with a series of questions.
inquirer.prompt([

  // Here we create a basic text prompt.
  {
    type: "input",
    message: "Player 1, What is your name?",
    name: "playerOneName"
  },

  // Here we give the user a list to choose from.
  {
    type: "list",
    message: "Which token do you choose? (use arrow keys and press enter to select)",
    choices: ["X", "O"],
    name: "token"
  },

  {
    type: "input",
    message: "Player 2, What is your name?",
    name: "playerTwoName"
  },

  // Here we ask the user to confirm.
  {
    type: "confirm",
    message: "Ready to play?",
    name: "confirm",
    default: true

  }

// Once we are done with all the questions... "then" we do stuff with the answers
// In this case, we store all of the answers into a "user" object that inquirer makes for us.
]).then(function(user) {


  // If we log that user as a JSON, we can see how it looks.
  console.log(JSON.stringify(user, null, 2));

  // If the user confirms, we display the users' names from the answers.
  if (user.confirm) {

    console.log("==============================================");
    console.log("");
    console.log("Welcome " + user.playerOneName + " and " + user.playerTwoName + " !");
    console.log("");
    console.log("==============================================");


  // If the user does not confirm, then a message is provided and the program quits.
  }

  else {

    console.log("");
    console.log("");
    console.log("That's okay, come again when y'all are more sure.");
    console.log("");
    console.log("");

  }

});
