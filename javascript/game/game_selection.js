// Load the NPM Package inquirer
var inquirer = require("inquirer");

var gameToPlay;

inquirer.prompt([

  {
    type: "list",
    message: "Welcome to Tic Tac Toe!! Which game would you like to play?",
    choices: ["Human vs Computer", "Human vs Human", "Human vs Human"],
    name: "gameType"
  }

]).then(function(gametype) {
	gameToPlay = gametype.gameType; 
    console.log(gameToPlay);
}); 

module.exports = gameToPlay;