
//initiate main board with blank spots to be filled with guess
let originalBoard = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '];

//declare players and respective token 
//todo: gather token from inquire prompts 
const humanPlayer = 'X';
const aiPlayer = '0';

//win combos from original game
const winCombos = [
  [(/OOO....../),'O'], 
  [(/...OOO.../),'O'], 
  [(/......OOO/),'O'], 
  [(/O..O..O../),'O'], 
  [(/.O..O..O./),'O'], 
  [(/..O..O..O/),'O'], 
  [(/O...O...O/),'O'], 
  [(/..O.O.O../),'O'], 
  [(/XXX....../),'X'], 
  [(/...XXX.../),'X'], 
  [(/......XXX/),'X'], 
  [(/X..X..X../),'X'], 
  [(/.X..X..X./),'X'], 
  [(/..X..X..X/),'X'], 
  [(/X...X...X/),'X'], 
  [(/..X.X.X../),'X']
]

//initiate current player for human vs computer game
//todo: allow human player set the order that the game starts
let currentPlayer = humanPlayer;

function computerTakesTurn() {
  //computerGuess is number calculated from minimax, number will be 
  //guessPosition argument for newPlayerTakesTurn
  let computerGuess = getNextAvailableSpace();
  newPlayerTakesTurn(computerGuess,aiPlayer);
  console.log('computer moved');
}

function newPlayerTakesTurn(guessPosition,player){
  //if current player has already taken a turn, return false so that the board can be 
  //checked to see if the board is full or checked to see if there is a winner
  if(player!=currentPlayer){
    return false;
  }
 
  // first element at open position between 1 and 8 will be replaced with x
  if(+guessPosition>=0&&+guessPosition<=8&&!isNaN(+guessPosition)&&originalBoard[+guessPosition] ==' '){
    originalBoard.splice(+guessPosition,1,player);
    currentPlayer = (player==aiPlayer)? humanPlayer: aiPlayer;
    return true;
  }
  return false;
}

//make a board from each position from the originalBoard array
function makeBoard(){
  return  ' '+originalBoard[0]+' |'+' '+originalBoard[1]+' |'+' '+
      originalBoard[2]+'\n===+===+===\n'+' '+originalBoard[3]+' |'+' '+
      originalBoard[4]+' |'+' '+originalBoard[5]+'\n===+===+===\n'+' '+
      originalBoard[6]+' |'+' '+originalBoard[7]+' |'+' '+originalBoard[8];
}

function displayBoard() {
  console.log(makeBoard());
}

function isBoardFilled() {
    x = emptySpots().length;
    if (x == 0){
      displayBoard();
      console.log("Tie Game!")
      //if there are no available spaces, return true for the game to close out
      return true;
    } 
  return false;
}


function checkWin() {

  //join board array and compare to winning combo
  let boardString = originalBoard.join('');
    let theWinner = null;
    for(i=0;i<winCombos.length;i++){
      //check the boardstring against the first entity of each winning combo
        winningArray = boardString.match(winCombos[i][0])
        if(winningArray){
          //if there is a match, the winner is the second entity of the winning combo array
          theWinner= winCombos[i][1]
        }
    }
    if(theWinner){
      console.log('Game over ' + theWinner)
      //if there is winner, return true for the game to close out
      return true;
      }
  return false;
}


function getNextAvailableSpace() {
  // should return the next best position to guess
  return minimax(originalBoard, aiPlayer).index; 
}

function emptySpots(board) {
  console.log('checking for empty spots')
  return board.filter(s => s == ' ');
}

function minimax(newBoard,player) {
  //assign number of empty spots left so as to compare the number of moves left
  var availableSpaces = emptySpots(newBoard);


  var winner = checkWin();
  //return a score for potential wins
  if (winner == humanPlayer) {
    return {score: -10};
  } else if (winner == aiPlayer) {
    return {score: 10};
  } else if (availableSpaces.length === 0) {
    return {score: 0};
  }

  var moves = [];
  for (var i = 0; i < availableSpaces.length; i++) {
    var move = {};
    move.index = newBoard[availableSpaces[i]];
    //for each available space, assign empty spot to current player
    newBoard[availableSpaces[i]] = player;
    
    //if the current player is the aiPlayer, take the minimax score and store it in the move object
    if (player == aiPlayer) {
      var result = minimax(newBoard, humanPlayer);
      move.score = result.score;
    } else {
      var result = minimax(newBoard, aiPlayer);
      move.score = result.score;
    }

    //reset position to empty
    newBoard[availableSpaces[i]] = move.index;

    moves.push(move);
  }

  var bestMove;
  if(player === aiPlayer) {
    var bestScore = -10000;
    for(var i = 0; i < moves.length; i++) {
      if (moves[i].score > bestScore) {
        bestScore = moves[i].score;
        bestMove = i;
      }
    }
  } else {
    var bestScore = 10000;
    for(var i = 0; i < moves.length; i++) {
      if (moves[i].score < bestScore) {
        bestScore = moves[i].score;
        bestMove = i;
      }
    }
  }
  return moves[bestMove];
}


function exitGame() {
  console.log('Thanks for playing!')
  process.exit();
}

function startGame() {
  displayBoard();
    console.log("Enter [0-8]:");
    process.openStdin().on('data',function(response){
      if(newPlayerTakesTurn(response, humanPlayer)){
          console.log('Thanks for taking your turn!');
          if(checkWin()||isBoardFilled()) {
              console.log('exiting game')
              exitGame();
          }
        } 
        else {
            console.log('computer goes next')
            calculateComputerMove();
            if (checkWin()||isBoardFilled()) {
                exitGame();  
        } 
        else displayBoard();
      }
    })
}

startGame();

module.exports = {emptySpots}
